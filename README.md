The purpose of this project was to integrate and fullfil course requirements grasped from the undergraduate course of **Introduction to Natural Language Processing** at Nile University, Egypt.

In a nutshell, the program:

1.	Listens to user
2.	Preprocess using NLP techniques
3. 	Prints out a class of interests and its score

The repository includes:

1. 	Code
2.	Documentation



